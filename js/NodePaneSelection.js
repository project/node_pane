(function($){
	Drupal.behaviors.NodePaneSelection = {
	  attach: function(context) {
	  	// hide submit button
	  	$('#ctools-node-content-type-selection-form').find('#edit-next').hide();
	  	// fill the ctools form with the selected node ID
	    $('.view-node-pane-selection a.node-pane-selector').click(function(){
	    	var selected_nid = $(this).find('span.node-selector').attr('data-nid');
				$('[name="selected_nid"]').val(selected_nid) ;
				$('#ctools-node-content-type-selection-form').submit();
	      return false;
	    });

	    // show only filters that are appropriate to the cointent type selection
	    
	    function hideContextualWidgets(widgetcat) {
	    	var contextual_widgets = {
		    	'news':'.views-widget-filter-field_news_category_tid', 
		    	'product':'.views-widget-filter-field_product_category_tid,.views-widget-filter-field_product_type_value',
		    	'media':'.views-widget-filter-field_media_category_tid',
		    	'page':'.views-widget-filter-field_page_category_tid',
		    	'publication':'.views-widget-filter-field_publication_type_tid'
		    };

    		for (key in contextual_widgets) {
    			if(key != widgetcat) {
    				$(contextual_widgets[key]).find('select').val('All');
    			}
		    	$(contextual_widgets[key]).hide();
		    }
    		$(contextual_widgets[widgetcat]).show();
	    }

	    hideContextualWidgets($('select#edit-type').val());
	    
	    $('select#edit-type').change(function() {
	    	hideContextualWidgets($(this).val()); 
				});
	    
	  }
	}
})(jQuery);